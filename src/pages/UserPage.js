import React from 'react'
import { Input } from 'antd';
import { List, Avatar, Button, Skeleton } from 'antd';
import reqwest from 'reqwest';


const count = 3;
const fakeDataUrl = `https://randomuser.me/api/?results=${count}&inc=name,gender,email,nat&noinfo`;

const { Search } = Input;


class UserPage extends React.Component {


    state = {
        users: [],
    };

    fetchUserData() {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(Response => Response.json())
            .then(data => {
                console.log(data);
                this.setState({ users: data })
            })
            .catch(error => console.log(error));
    }

    componentDidMount() {
        this.fetchUserData();
    }

    render() {
        const { list } = this.state;

        return (
            <div>
                {/* <h2>User Page : {this.match.params.name}</h2> */}
                <center>
                    <Search
                        placeholder="input search text"
                        enterButton="Search"
                        size="large"
                        style={{ width: 400 }}
                        onSearch={value => console.log(value)}
                    />
                </center>


                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    dataSource={this.state.users}
                    renderItem={item => (
                        <List.Item
                            actions={[<a href={"/users/" + item.id + "/todo"}>Todo</a>, <a key="list-loadmore-more">more</a>]}
                        >
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                    }
                                    title={<a href="https://ant.design">{item.name}</a>}
                                    description={item.email}
                                />
                                <div>{item.phone}</div>
                            </Skeleton>
                        </List.Item>
                    )}
                />
            </div>
        );
    }
}


export default UserPage;