import React, { useState, useEffect } from 'react'
import { Descriptions } from 'antd';
import { List, Typography } from 'antd';
import { Select } from 'antd';
import { Button } from 'antd';
import { Card } from 'antd';

const { Option } = Select;


const TodoPage = (props) => {

    const [user, setUser] = useState(undefined);
    const [todoList, setTodoList] = useState([]);

    const [filtertype, setFiltertype] = useState([]);
    const [filterlist, setFilterlist] = useState('ALL');


    const fetchUserdata = () => {

        const userId = props.match.params.user_id
        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(Response => Response.json())
            .then(data => {
                setUser(data);
            })
            .catch(error => console.log(error));
    }

    const fetchTododata = () => {

        const userId = props.match.params.user_id
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(Response => Response.json())
            .then(data => {
                setTodoList(data);
                filter(data)
            })
            .catch(error => console.log(error));
    }

    //Component did mount
    useEffect(() => {
        fetchUserdata();
        fetchTododata();
    }, [])

    const onClickDone = (index) => {
        let todoListTmp = [...todoList];
        todoListTmp[index].completed = true;
        setTodoList(todoListTmp);
    }

    const filter = (type) => {

        let filterItem = [];
        type.map((item) => {
            if (!filterItem.includes(item.completed)) {
                filterItem.push(item.completed)
            }
        });
        setFiltertype(filterItem)
    }

    const onfilter = (index) => {
        let keeptype = index
        setFilterlist(keeptype)
    }




    // console.log(user)

    console.log(todoList)
    return (
        <div>
            {
                user != undefined &&
                <center>
                    <div style={{ background: '#ECECEC', padding: '30px' }}>
                        <Card title={"User : " + user.name} bordered={false} style={{ width: 1200 }}>
                            <Descriptions>
                                <Descriptions.Item label="Username ">{user.username}</Descriptions.Item>
                                <Descriptions.Item label="Email ">{user.email}</Descriptions.Item>
                                <Descriptions.Item label="Phone ">{user.phone}</Descriptions.Item>
                                <Descriptions.Item label="Address">Street {user.address.street}. , Suite {user.address.suite} , City {user.address.city} , Zipcode {user.address.zipcode}.</Descriptions.Item>
                            </Descriptions>
                        </Card>
                    </div></center>
            }


            <center><div>


                <Card title="Todo List" extra={<Select defaultValue="ALL" style={{ width: 120 }} onChange={onfilter}>

                    <Option value='ALL'>ALL</Option>
                    <Option value={0}>DOING</Option>
                    <Option value={1}>DONE</Option>

                </Select>} style={{ width: 1500 }}>
                    
                    <List

                        footer={<div>Total items : {todoList.length} </div>}
                        bordered
                        dataSource={
                            filterlist == 'ALL' ?
                                todoList
                                :
                                todoList.filter(m => m.completed == filtertype[filterlist])
                        }
                        renderItem={(item, index) => (

                            <List.Item >

                                <Typography.Text mark={!item.completed} delete={item.completed} >
                                    {
                                        !item.completed &&
                                        <Button style={{ float: 'right' }} type="primary" ghost onClick={() => { onClickDone(index) }}>DONE</Button>
                                    }
                                    {
                                        item.completed == false ?
                                            "DOING "
                                            :
                                            "DONE "
                                    }</Typography.Text>{item.title}

                            </List.Item>

                        )}
                    />
                </Card>
            </div></center>


        </div>

    )
}

export default TodoPage;