import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter , Route} from 'react-router-dom'
import UserPage from './pages/UserPage';
import TodoPage from './pages/TodoPage';

function App() {
  return (
    <BrowserRouter>
      <Route path="/users" component={UserPage} exact={true}></Route>
      <Route path="/users/:user_id/todo" component={TodoPage}></Route>
    </BrowserRouter>
  );
}

export default App;
